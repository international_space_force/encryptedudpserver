#ifndef UDPENCRYPTEDSERVER_UDPSERVER2_H
#define UDPENCRYPTEDSERVER_UDPSERVER2_H

#include <string>
#include <netinet/in.h>
#include <netdb.h>

class UDPServer2 {
private:
        std::string port;
        std::string address;
        std::string key;

        int sock;
        char buffer[3000];

        struct addrinfo addrCriteria;
        struct addrinfo *servAddr; // List of server addresses
        
public:
    UDPServer2(std::string serverAddress, std::string serverPort, std::string serverKey) {
        address = serverAddress;
        port = serverPort;
        key = serverKey;

        memset(&addrCriteria, 0, sizeof(addrCriteria)); // Zero out structure
        addrCriteria.ai_family = AF_UNSPEC;             // Any address family
        addrCriteria.ai_flags = AI_PASSIVE;             // Accept on any address/port
        addrCriteria.ai_socktype = SOCK_DGRAM;          // Only datagram socket
        addrCriteria.ai_protocol = IPPROTO_UDP;         // Only UDP socket
    }

    bool  IsInitializeConnection() {
        bool result = false;

        int rtnVal = getaddrinfo(NULL, port.c_str(), &addrCriteria, &servAddr);
        if (rtnVal == 0) {
            // Create socket for incoming connections
            sock = socket(servAddr->ai_family, servAddr->ai_socktype, servAddr->ai_protocol);
            if (sock >=  0) {
                // Bind to the local address
                if (bind(sock, servAddr->ai_addr, servAddr->ai_addrlen) >= 0) {
                    result = true;
                    // Free address list allocated by getaddrinfo()
                    freeaddrinfo(servAddr);
                } else {
                    printf("bind() failed");
                }
            } else {
                printf("socket() failed");
            }
        } else {
            printf("getaddrinfo() failed: %s", gai_strerror(rtnVal));
        }

        return result;
    }


};


#endif //UDPENCRYPTEDSERVER_UDPSERVER2_H

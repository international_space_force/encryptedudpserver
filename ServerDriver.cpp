#include <cstdio>
#include <vector>
#include <iostream>
#include "UDPServer.hpp"
#include "ConcurrentQueue.hpp"
#include "UDPClient.hpp"

const int DATALIMITSIZE = 3000;
const int KEYSIZE = 3;
char const key[KEYSIZE] = "YO";

std::vector<char> encryptData(std::vector<char> rawData, std::vector<char> key) {
    std::vector<char> encryptedData({});

    for(int index = 0; index < rawData.size(); index++) {
        if (rawData[index] != '\n')
            encryptedData.push_back((char)rawData[index] ^ key[index % key.size()]);
        else
            encryptedData.push_back(rawData[index]);
    }

    return encryptedData;
}

void ApplyXorBitMaskToData(int rawDataCount, char rawData[], char maskedAppliedData[]) {
    for (int i = 0; i < rawDataCount - 1; i++) {
        if (rawData[i] != '\n') {
            int keyModuloIndex = i % KEYSIZE;
            maskedAppliedData[i] = (char) (rawData[i] ^ key[keyModuloIndex]);
        }
        else
            maskedAppliedData[i] = rawData[i];
    }
}

std::vector<char> CharArrayToVector(char* inputData, int inputDataSize) {
    std::vector<char> result(inputData, inputData + inputDataSize);
    return result;
}

void VectorTocharArray(std::vector<char> inputData, char* outputData) {
    if(inputData.size() <= DATALIMITSIZE) {
        std::copy(inputData.begin(), inputData.end(), outputData);
    }
}

void WaitForDataToBeReceived(UDPServer& serverConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATALIMITSIZE];
        int receivedByteCount = 0;

        printf("Waiting for connection: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATALIMITSIZE);
        printf("Received: %s\n", receivedBuffer);

        queue.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

void SendDataFromQueue(UDPClient& clientModuleConnection, ConcurrentQueue<std::vector<char>>& queue) {
    while(true) {
        char receivedBuffer[DATALIMITSIZE];
        char xorData[DATALIMITSIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        queue.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();

        VectorTocharArray(dataToBeSent, receivedBuffer);

        ApplyXorBitMaskToData(receivedByteCount, receivedBuffer, xorData);
        printf("Applying mask: %s\n", xorData);

        printf("Sending Masked Data\n");
        clientModuleConnection.SentDatagramToServerSuccessfully(xorData, receivedByteCount);
    }
}

int main()
{
    ConcurrentQueue<std::vector<char>> dataQueue;
    UDPServer server1(5004, "127.0.0.1");
    UDPClient clientToModuleSender(5005, "127.0.0.1");

    std::thread receiveData(WaitForDataToBeReceived, std::ref(server1), std::ref(dataQueue));
    std::thread sendData(SendDataFromQueue, std::ref(clientToModuleSender), std::ref(dataQueue));
    receiveData.join();
    sendData.join();

    return 0;
}